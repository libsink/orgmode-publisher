;;; package --- publish-hypertext

;; Copyright 2021 Koustubh Sinkar

;;; Commentary:
;; exports org files to html standard

;;; License 🄯
;; This file is part of orgmode-publisher.
;;
;; Orgmode-publisher is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License.
;;
;; Orgmode-publisher is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public License
;; along with orgmode-publisher.  If not, see <https://www.gnu.org/licenses/>.

;;; Code:

(require 'ox-publish)
(setq org-publish-project-alist
      '(("html"
         :base-directory "org/"
         :base-extension "org"
         :publishing-directory "publications/html"
         :recursive t
         :publishing-function org-html-publish-to-html
         :headline-levels 6             ; Just the default for this project.
         :html-validation-link nil
         :html-html5-fancy t
         :html-indent t
         :html-doctype "html5"
         :html-container "main"
         :html-content-class "article"
         :html-head-include-scripts nil
         :html-head-include-default-style nil
         :html-head-extra "<meta http-equiv=\"content-security-policy\"
                                 content=\"default-src 'none';
                                           style-src 'self';
                                           font-src 'self';
                                           img-src 'self';
                                           base-uri 'self'\" />
                            <link rel=\"stylesheet\" type=\"text/css\" href=\"assets/stylesheets/main.css\" />
                            <link rel=\"icon\" type=\"image/x-icon\" href=\"assets/icons/favicon.png\" />"
         :auto-preamble t)
        ("assets"
         :base-directory "assets/"
         :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf"
         :publishing-directory (expand-file-name "publications/html/assets/")
         :recursive t
         :publishing-function org-publish-attachment)
        ("hypertext" :components ("html" "assets"))
        ))

(org-publish-project "hypertext")

;;; Footer
(provide 'publish-hypertext)
;;; publish-hypertext ends here
