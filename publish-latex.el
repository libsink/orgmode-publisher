;;; publish-latex --- convert org-files to latex

;; Copyright 2021 Koustubh Sinkar

;;; Commentary:
;; Should convert org markup to latex source

;;; License 🄯
;; This file is part of orgmode-publisher.
;;
;; Orgmode-publisher is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Lesser General Public License as published by
;; the Free Software Foundation, either version 3 of the License.
;;
;; Orgmode-publisher is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public License
;; along with orgmode-publisher.  If not, see <https://www.gnu.org/licenses/>.

;;; Code:

(require 'ox-publish)
(setq org-publish-project-alist
      '(("latex"
         :base-directory "org/"
         :base-extension "org"
         :publishing-directory "publications/latex"
         :recursive t
         :publishing-function org-latex-publish-to-latex
         :headline-levels 6             ; Just the default for this project.
         :auto-preamble t)
        ("assets"
         :base-directory "assets/"
         :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|woff2\\|ico"
         :publishing-directory "publications/latex/assets/"
         :recursive t
         :publishing-function org-publish-attachment)
        ("latex" :components ("latex"))
        ))

(org-publish-project "latex")

;;; Footer
(provide 'publish-latex)
;;; publish-latex ends here
